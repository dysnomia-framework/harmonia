local config
have_cfg=false

get=->
  return config if config and have_cfg
  if _G.harmonia_config
    config=_G.harmonia_config
    have_cfg='global'

  if not config
    pcall ->
      config = require 'dysnomia.config'
      if config.harmonia
        config=config.harmonia
        have_cfg='dysnomia'
      else
        config=nil

  if not config
    pcall ->
      config = require 'harmonia_config'
      have_cfg = 'file'

  return config

{
  make: ->
    config={}
    have_cfg='explicit'
    get!
  :get
  which:-> have_cfg
}
