config = require("harmonia.config").get!
if config.postgres
  require "harmonia.postgres.schema"
elseif config.mysql
  require "harmonia.mysql.schema"
else
  error "You have to configure either postgres or mysql"
