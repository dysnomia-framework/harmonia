config = require("harmonia.config").get!
if config.postgres
  require "harmonia.postgres"
elseif config.mysql
  require "harmonia.mysql"
else
  error "You have to configure either postgres or mysql"
