config = require("harmonia.config").get!
if config.postgres
  require "harmonia.postgres.model"
elseif config.mysql
  require "harmonia.mysql.model"
else
  error "You have to configure either postgres or mysql"
