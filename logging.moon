import insert from table

flatten_params_helper = (params, out = {}, sep= ", ")->
  return {"{}"} unless params

  insert out, "{ "
  for k,v in pairs params
    insert out, tostring k
    insert out, ": "
    if type(v) == "table"
      flatten_params v, out
    else
      insert out, ("%q")\format v
    insert out, sep

  -- remove last ", "
  out[#out] = nil if out[#out] == sep

  insert out, " }"
  out

flatten_params= (params) ->
  table.concat flatten_params_helper params

tbl={
  :flatten_params
}

meta={
  __index: (k)=>
    (...)->
      print 'LOG['..k..']: ', flatten_params {...}
}

setmetatable(tbl,meta)